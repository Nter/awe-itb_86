#include <iostream>
#include <windows.h>

using namespace std;

int fibonacci(int zahl) {
	Sleep(620);
	if (zahl == 0) {
		return 0;
	}

	if (zahl == 1) { 
		return 1;
	}
	return fibonacci(zahl - 1) + fibonacci(zahl - 2);
}

int fakultaet(unsigned int zahl) {
	Sleep(620);
	if (zahl <= 1) {
		return 1;
	}
	else {
		return zahl * fakultaet(zahl - 1);
	}
}


int main() {
	int zahl;
	cout << "Fibonacci: " << endl;
	Sleep(220);
	cout << "Bitte Zahl eingeben: ";
	cin >> zahl;                                 
	cout << "Die Fibonacci-Zahl von " << zahl << " ist " << fibonacci(zahl) << "." << endl;


	cout << "Fakultaet: " << endl;
	Sleep(320);
	zahl = 0;
	cout << "Bitte Zahl eingeben: ";
	cin >> zahl;
	cout << "Die Fakultaet von " << zahl << " ist " << fakultaet(zahl) << "." << std::endl;
}