#include <iostream>
using namespace std;

int main() {
	/* Aufgabe 1
	int i = 0, k = 0, kmax = 10;
	for (i = 10; i >= 0; i--) {
		for (k = kmax; k >= 0; k--) {
			cout << k << " ";
		}
		cout << endl;
		kmax--;
	}
	*/

	///* Aufgabe 2
	int i,j;
	bool err = false;
	cout << "Geben Sie eine Zahl ein: " << endl;
	cin >> i;
	if (i == 1) err = true;
	for (j = 2; j < i && !err; j++) {
		if (i % j == 0) {
			err = true;
		}
	}
	if (!err) cout << i << " ist eine Primzahl!" << endl;
	else cout << "Keine Primzahl!" << endl;
	//*/

	/* Aufgabe 3
    
	// switches
    bool s1 = false,
        s2 = false,
        s3 = false,
        s4 = false,
        s5 = false;

    // helper
    int maxSwitches = 5;

    // output header and all-false-case
    cout << "s1\t" << "s2\t" << "s3\t" << "s4\t" << "s5\t"<< "LED" << endl;
    cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;

    // loop over all switches
    for(int i = 1; i <= maxSwitches; i++) {
        s1 = (i==1);
        s2 = (i==2);
        s3 = (i==3);
        s4 = (i==4);
        s5 = (i==5);
        
        cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
        
        // for every switch loop over all remaining switches
        for(int j = i+1; j <= maxSwitches; j++) {
            s1 = ((i==1) || (j==1));
            s2 = ((i==2) || (j==2));
            s3 = ((i==3) || (j==3));
            s4 = ((i==4) || (j==4));
            s5 = ((i==5) || (j==5));
            
            cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
            
            // same procedure as every for, james
            for(int k = j+1; k <= maxSwitches; k++) {
                s1 = ((i==1) || (j==1) || (k==1));
                s2 = ((i==2) || (j==2) || (k==2));
                s3 = ((i==3) || (j==3) || (k==3));
                s4 = ((i==4) || (j==4) || (k==4));
                s5 = ((i==5) || (j==5) || (k==5));
                
                cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
            
                // you guessed it
                for(int l = k+1; l <= maxSwitches; l++) {
                    s1 = ((i==1) || (j==1) || (k==1) || (l==1));
                    s2 = ((i==2) || (j==2) || (k==2) || (l==2));
                    s3 = ((i==3) || (j==3) || (k==3) || (l==3));
                    s4 = ((i==4) || (j==4) || (k==4) || (l==4));
                    s5 = ((i==5) || (j==5) || (k==5) || (l==5));
                    
                    cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
                    
                    // ...and another one!
                    for(int m = l+1; m <= maxSwitches; m++) {
                        s1 = ((i==1) || (j==1) || (k==1) || (l==1) || (m==1));
                        s2 = ((i==2) || (j==2) || (k==2) || (l==2) || (m==2));
                        s3 = ((i==3) || (j==3) || (k==3) || (l==3) || (m==3));
                        s4 = ((i==4) || (j==4) || (k==4) || (l==4) || (m==4));
                        s5 = ((i==5) || (j==5) || (k==5) || (l==5) || (m==5));
                        
                        cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
                        
                        // last one, I promise!
                        for(int n = m+1; n <= maxSwitches; n++) {
                            s1 = ((i==1) || (j==1) || (k==1) || (l==1) || (m==1) || (n==1));
                            s2 = ((i==2) || (j==2) || (k==2) || (l==2) || (m==2) || (n==2));
                            s3 = ((i==3) || (j==3) || (k==3) || (l==3) || (m==3) || (n==3));
                            s4 = ((i==4) || (j==4) || (k==4) || (l==4) || (m==4) || (n==4));
                            s5 = ((i==5) || (j==5) || (k==5) || (l==5) || (m==5) || (n==5));
                            
                            cout << s1 << "\t" << s2 << "\t" << s3 << "\t" << s4 << "\t" << s5 << "\t" << ((s3 || s4) && s5) << endl;
                        }
                    }
                }
            }
        }
    }

	//*/
	return 1;
}