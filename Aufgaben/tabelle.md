# Entscheidungstabelle
**Bedingung 1:**
bisherige Zahlungsverhalten einwandfrei
**Bedingung 2:**
bisherige Zahlungsverhalten nicht einwandfrei
**Bedingung 3:**
vereinbarte Kreditgrenze nicht überschritten
**Bedingung 4:**
Überschreitungsbetrag < 500€
**Bedingung 5:**
Überschreitungsbetrag > 500€

| Kreditgrenze          | R1 | R2 | R3 | R4 | R5|R6 |
|-----------------------|----|----|----|----|---|---|
|Bedingung 1            |  x |  x |    |    | 	|   |
|Bedingung 2            |    |    |  x |  x | 	|   |
|Bedingung 3            |    |    |    |    |x  |x  |
|Bedingung 4            | x  |    |  x |    |x	|   |
|Bedingung 5		|    |  x |    |  x |	|x  |
|**Aktionen**           |    |    |    |    |	|   |
| Scheck einlösen       |  x |  x |    |    |	|   |
| neue Konditionen      |    |  x |    |    |	|   |
| Scheck nicht einlösen |    |    |    |    |	|   |
